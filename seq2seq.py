# -*- coding: utf-8 -*-
from __future__ import absolute_import
from __future__ import print_function

import sys
import codecs
import numpy as np
import os.path
import random
import operator
import cPickle as pkl
import yaml
import argparse

import theano
from keras.preprocessing import sequence
from keras.preprocessing.text import one_hot
from text import Tokenizer
from keras.optimizers import SGD, RMSprop, Adagrad
from keras.utils import np_utils
from keras.models import Sequential
from keras.layers.core import TimeDistributedDense, Dense, Dropout, Activation, RepeatVector
from keras.layers.embeddings import Embedding
from keras.layers.recurrent import LSTM, GRU
from keras.callbacks import ModelCheckpoint, Callback
from birnn import BiDirectionLSTM, Transform


class LossHistory(Callback):
    def on_train_begin(self, logs={}):
        self.losses = []

    def on_batch_end(self, batch, logs={}):
        self.losses.append(logs.get('loss'))
    
    def on_epoch_end(self, epoch, logs={}):
        f = file(fdir+'/losses.pkl', 'wb')
        pkl.dump(history.losses, f, protocol=pkl.HIGHEST_PROTOCOL)
        f.close()
        
class Sample(Callback):
    #ugly hack
    def on_epoch_end(self, epoch, logs={}):
        if char:
            lim=""
        else:
            lim=" "
        
        X_eval = X_batch[:int(len(X_data)*0.9)]
        a = model.predict_classes(X_eval[:5],batch_size=1, verbose=1)
        for i,sent in enumerate(a):
            print('original: ',)   
            for word in X_eval[i]:
                if word in X_vocab:
                    sys.stdout.write(X_vocab[word]+lim)
            print('\n')
            print('generated: ',)
            for word in sent:
                if word in Y_vocab:
                    sys.stdout.write(Y_vocab[word]+lim)
            print('\n')
 
def to_char(data,flag):
    if flag:
        list=[]
        for line in data:
            #line = line.decode('utf-8')
            list.append(" ".join(line))#.encode('utf-8'))
        return list
    else:
        return data
         
def vectorize(data, train, max_words):#make X vectors out of list of sentences
    tokenizer = Tokenizer(nb_words=max_words, filters='\n',split=" ")
    tokenizer.fit_on_texts(train)
    data = tokenizer.texts_to_sequences(data)
    data = [element or str(max_words-1) for element in data]
    index = tokenizer.word_index
    inv_index = {v: k for k, v in index.items()}
    inv_index[0]=" "
    inv_index[max_words-1]="<unk>"
    return data,inv_index#,y_data


def shuffle_two(a, b):
    assert len(a) == len(b)
    p = np.random.permutation(len(a))
    return a[p], b[p]

def pad(vecsx,vecsy):
    #pad to max len and flip
    lx = len(max(vecsx,key=len))
    ly = len(max(vecsy,key=len))
    #to do the seq2seq flip trick:
    #datax = np.fliplr(sequence.pad_sequences(vecsx,maxlen=lx))
    datax = sequence.pad_sequences(vecsx,maxlen=lx)
    datay = sequence.pad_sequences(vecsy,maxlen=ly)
    datax,datay=shuffle_two(datax,datay)
    print ("Chunks have shape "+str(datax.shape)+str(datay.shape))
    return datax,datay

def chunks(lx,ly, n):
    #split data in chunks in order for vectors to fit in mem 
    n = max(1, n)
    outx=[]
    outy=[]
    chunks_X = [lx[i:i + n] for i in range(0, len(lx), n)]   
    chunks_Y = [ly[i:i + n] for i in range(0, len(ly), n)]   
    for i,chunkx in enumerate(chunks_X):
        chunkx,chunky = pad(chunks_X[i],chunks_Y[i])
        outx.append(chunkx)
        outy.append(chunky)
    return outx,outy

def one_hot(data, max_features):
    print ("Generating one hot vectors..")
    max_len = len(data[0])
    y_data = np.zeros((len(data), max_len, max_features))

    for i, sentence in enumerate(data):
        for t, word in enumerate(sentence):
            y_data[i, t, word] = 1
    #print(y_data.shape)
    return y_data

def get_activations(model, layer, X_batch):
    get_activations = theano.function([model.layers[0].input], model.layers[layer].get_output(train=False), allow_input_downcast=True)
    activations = get_activations(X_batch)
    return activations

parser = argparse.ArgumentParser(description='Encoder Decoder')
parser.add_argument('-d','--dir', help='Directory which contains your experiment files (cfg.yaml, aligned source and target text files)', required=True)
parser.add_argument('-m','--mode', help='Mode in which to start seq2seq, either train or encode', required=False, default='train')

args = vars(parser.parse_args())
fdir=args['dir']
mode = args['mode']

with open(fdir+'/cfg.yaml', 'r') as f:
    cfg = yaml.load(f)

#Files
source = cfg["files"]["source"]
target = cfg["files"]["target"]
if mode =="encode":
    to_encode = cfg["files"]["encode"]
#Neural net
max_features_X = cfg["neural_net"]["vocab_source"]
max_features_Y = cfg["neural_net"]["vocab_target"]
embedding_size = cfg["neural_net"]["embedding_size"]
nb_layers = cfg["neural_net"]["hidden_layers"]
hidden_size = cfg["neural_net"]["hidden_size"]
dropout = cfg["neural_net"]["dropout"]
maxlen = cfg["neural_net"]["max_len"]
#Settings
char = cfg["settings"]["char_model"]
split = 1-cfg["settings"]["test_split"]
chunk_size = cfg["settings"]["batch_size"]
batch_size = cfg["settings"]["mini_batch_size"]
nb_epoch = cfg["settings"]["epochs"]
seed = cfg["settings"]["seed"]
np.random.seed(seed)

#DATA PREP#
X_traindata = to_char(open(fdir+'/'+source).readlines(),char)
Y_traindata = to_char(open(fdir+'/'+target).readlines(),char)


if mode =="encode":
    to_encode = to_char(open(fdir+'/'+to_encode).readlines(),char)
    X_data,X_vocab = vectorize(to_encode, X_traindata, max_features_X)
    Y_data,Y_vocab = X_data,X_vocab
    batches_X,batches_Y = chunks(X_data,Y_data, len(X_data))
elif mode == "train":
    X_data,X_vocab = vectorize(X_traindata, X_traindata, max_features_X)
    Y_data,Y_vocab = vectorize(Y_traindata, Y_traindata, max_features_Y)
    #X_data,Y_data= shuffle_two(X_data,Y_data)
    X_train = X_data[:int(len(X_data)*split)]
    Y_train = Y_data[:int(len(Y_data)*split)]
    X_train, Y_train = zip(*sorted(zip(X_train, Y_train),key=lambda pair: len(pair[0])+len(pair[1])))
    #leave out possible long anomalies
    #X_train = X_train[:int(len(X_train)*0.95)]
    #Y_train = Y_train[:int(len(Y_train)*0.95)]
    #chunk it up
    batches_X,batches_Y = chunks(X_train,Y_train, chunk_size)


#BUILDING MODEL#
#print('Building model...')
model = Sequential()
model.add(Embedding(max_features_X, embedding_size, mask_zero=False))
var_size=embedding_size
for i in range(nb_layers):
    if i == nb_layers-1:
           return_s=False
    else:
        return_s=True
    model.add(BiDirectionLSTM(var_size, hidden_size, output_mode='sum', return_sequences=return_s))
    model.add(Dropout(dropout))
    var_size=hidden_size
    return_s=False
model.add(Dense(hidden_size, hidden_size))
model.add(Activation('relu'))
model.add(RepeatVector(maxlen))
for i in range(nb_layers):
    model.add(LSTM(hidden_size, hidden_size, return_sequences=True))
    model.add(Dropout(dropout))
model.add(TimeDistributedDense(hidden_size, max_features_Y))
model.add(Activation('time_distributed_softmax'))

if os.path.exists(fdir+'/weights.hdf5'):
    model.load_weights(fdir+'/weights.hdf5')

#todo: add option to choose other optimizer in cfg
rmsprop=RMSprop(lr=0.0002, rho=0.99, epsilon=1e-8, clipnorm=5)    
model.compile(loss='categorical_crossentropy', optimizer='rmsprop')

#save all checkpoints        
checkpointer = ModelCheckpoint(filepath=fdir+"/weights.hdf5", verbose=1, save_best_only=False)
history = LossHistory()
sample = Sample()

if mode=='train':
    #TRAINING#
    print("Training...")
    
    for e in range(nb_epoch):
        #for X_batch,Y_batch in zip(batches_X,batches_Y):
        for i, batch in enumerate(batches_X):
            X_batch= batches_X[i]
            Y_batch = one_hot(batches_Y[i],max_features_Y)
            print("epoch %d batch %d" % (e,i))
            model.fit(X_batch, Y_batch, batch_size=batch_size, nb_epoch=1, validation_split=0.1, callbacks=[checkpointer,history,sample])
            f = file(fdir+'/losses.pkl', 'wb')
            pkl.dump(history.losses, f, protocol=pkl.HIGHEST_PROTOCOL)
            f.close()

elif mode == 'encode':
    
    activations = model._predict(batches_X[0])
    for sen in activations:
        print (sen)
    
    #activ = get_activations(model,5,batches_X[0])
    #for sen in activ:
    #    d= np.amax(sen, axis=0)
    #    for i in d:
    #        print(str(i)+" ", end="")
    #    print(' ')