from scipy import spatial
import numpy as np
import sys

def getVecs(file):
    x=[]
    vecs = open(file).readlines()
    for i in vecs:
        x.append(i.split())
    x=np.array(x,dtype=float)
    return x


file1=sys.argv[1]
file2=sys.argv[2]

vecs1=getVecs(file1)
vecs2=getVecs(file2)

for n,i in enumerate(vecs1):
    result = 1 - spatial.distance.cosine(vecs1[n], vecs2[n])
    
    
    if result>0.5:
        print'true\t'+("%.4f" % result)
    elif result<0:
        print'false\t'+("%.4f" % 0)
    else:
        print'false\t'+("%.4f" % result)
        